package server;

import java.net.InetSocketAddress;

public class ChatPair {
	// This class defines chat pairs with 2 IP addresses.

	private String ip1 = "";
	private String ip2 = "";

	public ChatPair(String i1, String i2) {
		ip1 = i1;
		ip2 = i2;
	}

	public String getIP1() {
		return ip1;
	}

	public String getIP2() {
		return ip2;
	}

	// If you call this on a ChatPair instance, you can supply one address and you
	// will get the other one.
	public String getPeerAddress(String a) {
		if (a.equals(ip1)) {
			return ip2;
		}

		else if (a.equals(ip2)) {
			return ip1;
		}

		return null;
	}

	// If you call this it will look through the chatPairs ArrayList from the class
	// NetChet for an IP address. It will return the first chat pair that contains
	// that IP address.
	public static int getChatPairIDFromIP(String a) {
		for (int i = 0; i < NetChet.chatPairs.size(); i++) {
			if (NetChet.chatPairs.get(i).getIP1().equals(a) || NetChet.chatPairs.get(i).getIP2().equals(a)) {
				return i;
			}
		}

		return -1;
	}

	// This uses getChatPairIDFromIP to get the ID, then it will get the peer
	// address of that ID and return it.
	public static String findPeerAddress(String a) {
		int evaluation = getChatPairIDFromIP(a);

		if (evaluation != -1) {
			return NetChet.chatPairs.get(evaluation).getPeerAddress(a);
		}

		return null;
	}

	// This will get you the socket ID from ArrayList clients of the class NetChet
	// if you give it an address.
	public static int getSocketIDFromAddress(String a) {
		for (int i = 0; i < NetChet.clients.size(); i++) {
			String remoteSocketAddress = (((InetSocketAddress) NetChet.clients.get(i).getRemoteSocketAddress())
					.getAddress()).toString().replace("/", "");

			if (remoteSocketAddress.equals(a)) {
				return i;
			}
		}

		return -1;
	}
}