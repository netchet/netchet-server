package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class NetChet {
	private static String brandName = "NetChet Server";
	private static String brandVersion = "V2.0";
	private static String brandNameVersion = brandName + " " + brandVersion;

	private static SimpleDateFormat dateFormatter;
	private static Date currentDate;

	private static Node n;

	private static ServerSocket serverSocket;

	private static String myIP = "";

	private static boolean serverStatusError = false;
	private static boolean receiveMessagesError = false;

	// ArrayLists with list of clients and chatPairs.
	public static ArrayList<Socket> clients = new ArrayList<Socket>();
	public static ArrayList<ChatPair> chatPairs = new ArrayList<ChatPair>();

	public static void main(String[] args) {
		System.out.println("[INFO]: Starting " + brandNameVersion + "...");

		// Get IP address
		n = new Node();
		myIP = n.getIP();
		System.out.println("[INFO]: Your IP is " + myIP);

		connect();
	}

	public static void connect() {
		System.out.println("[INFO]: Starting server on port 33333...");

		try {
			serverSocket = new ServerSocket(33333);
			serverSocket.setSoTimeout(100000);

			Runnable serverSocketAccept = new Runnable() {
				public void run() {
					while (true) {
						try {
							// Always do this: add new people to clients
							clients.add(serverSocket.accept());
						} catch (IOException e) {
							if (serverStatusError == false) {
								System.err.println("[ERR!]: An unknown error occurred!");
								System.err.println("[ERR!]: Check your Internet connection!");
								e.printStackTrace();
								serverStatusError = true;
							}
						}
					}
				}
			};

			new Thread(serverSocketAccept).start();

			Runnable runServer = new Runnable() {
				public void run() {
					while (true) {
						// Always do this: chat logic

						// Do this for every client
						for (int i = 0; i < clients.size(); i++) {
							// Have they sent anything yet?
							String currentMessage = receiveMessage(clients.get(i));

							// If not, we don't need to do anything about them
							if (currentMessage == null || currentMessage.equals(null)) {
								break;
							}

							// Get their IP address
							String remoteSocketAddress = (((InetSocketAddress) clients.get(i).getRemoteSocketAddress())
									.getAddress()).toString().replace("/", "");

							// Do they want a new chat?
							if (currentMessage.contains("-----NETCHET CHATPAIR REQUEST-----")) {
								// Are they already in a chat?
								if (ChatPair.findPeerAddress(remoteSocketAddress) == null
										|| ChatPair.findPeerAddress(remoteSocketAddress).equals(null)
										|| ChatPair.findPeerAddress(currentMessage.substring(34)) == null
										|| ChatPair.findPeerAddress(currentMessage.substring(34)).equals(null)) {
									// They are not, so we can add them.
									chatPairs.add(new ChatPair(remoteSocketAddress, currentMessage.substring(34)));
								}
							}

							// Otherwise we just forward messages between them:
							else {
								/*
								 * This variable contains the returned value of sendMessage(). Send message
								 * needs two arguments: the message and the socket used to send it. The socket
								 * used to send it is determined by: 1. What is the peer address (to whom shall
								 * the message go)? 2. Look that IP up in the sockets list and get the socket ID
								 * 3. Get the socket at that ID from the sockets list.
								 */
								int sendMessageEvaluation = sendMessage(currentMessage, clients.get(ChatPair
										.getSocketIDFromAddress(ChatPair.findPeerAddress(remoteSocketAddress))));

								// Error? This happens when someone leaves. Remove both people.
								if (sendMessageEvaluation == 1 || sendMessageEvaluation == 2) {
									clients.remove(i);
									clients.remove(ChatPair
											.getSocketIDFromAddress(ChatPair.findPeerAddress(remoteSocketAddress)));

									// Remove all chat pairs the person has been in.
									while (ChatPair.getChatPairIDFromIP(remoteSocketAddress) != -1) {
										chatPairs.remove(ChatPair.getChatPairIDFromIP(remoteSocketAddress));
									}

									/*
									 * Also remove all chat pairs the other person has been in. People are not
									 * supposed to be in multiple chat pairs, but sometimes it can happen, so we
									 * better be safe.
									 */
									while (ChatPair
											.getChatPairIDFromIP(ChatPair.findPeerAddress(remoteSocketAddress)) != -1) {
										chatPairs.remove(ChatPair
												.getChatPairIDFromIP(ChatPair.findPeerAddress(remoteSocketAddress)));
									}
								}
							}
						}

						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							System.err.println("[ERR!]: Thread was interrupted while receiving a message!");
							e.printStackTrace();
						}
					}
				}
			};

			new Thread(runServer).start();
		} catch (SocketTimeoutException e) {
			System.out.println("[INFO]: Waiting for incoming connections...");

		} catch (IOException e) {
			System.err.println("[ERR!]: An unknown error occurred!");
			System.err.println("[ERR!]: Check your Internet connection!");
			e.printStackTrace();
		}
	}

	public static int sendMessage(String m, Socket toSocket) {
		try {
			// This is very simple and just sends it.
			DataOutputStream out = new DataOutputStream(toSocket.getOutputStream());
			out.writeUTF(m);
		} catch (IOException e) {
			e.printStackTrace();
			return 1;
		} catch (NullPointerException e) {
			e.printStackTrace();
			return 2;
		}

		return 0;
	}

	public static String receiveMessage(Socket fromSocket) {
		try {
			DataInputStream in = new DataInputStream(fromSocket.getInputStream());

			String receivedMessage = in.readUTF();
			return receivedMessage;
		} catch (IOException e) {
			if (receiveMessagesError == false) {
				e.printStackTrace();
				receiveMessagesError = true;
			}

			return null;
		} catch (NullPointerException e) {
			if (receiveMessagesError == false) {
				e.printStackTrace();
				receiveMessagesError = true;
			}

			return null;
		}
	}
}