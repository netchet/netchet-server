package server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Node {
	private String ip;

	public Node() {
		try {
			// Get the IP address by connecting to Cloudflare DNS servers and getting the IP
			// used to connect.
			// This is just like the client.
			Socket getIPSocket = new Socket();
			getIPSocket.connect(new InetSocketAddress("1.1.1.1", 80));
			ip = getIPSocket.getLocalAddress().toString().substring(1);
			getIPSocket.close();
		} catch (IOException e) {
			System.err.println("[ERR!]: This computer does not have any IP address!");
			System.err.println("[ERR!]: Check your Internet connection!");
			e.printStackTrace();
		}

	}

	public String getIP() {
		return ip;
	}
}